

#include <iostream>
#include "cashRegister.h"
#include "dispenserType.h"
#include <fstream>
 
using namespace std; 

const int NUMBEROFDISPENSERS = 8;

void showSelection();
void sellProduct(dispenserType& product, 
                 cashRegister& pCounter);
bool checkCurrency(int amount);
void fileReport(cashRegister& pCounter, dispenserType* array[]);

int main()
{
    cashRegister counter;
    dispenserType orange(60, 100); 
    dispenserType apple(70, 100);
    dispenserType mango(80, 45);
    dispenserType strawberryBanana(100, 85);
	dispenserType cranApple(100, 85);
	dispenserType cranGrape(100, 75);
	dispenserType crackers(100, 65);
	dispenserType chips(100, 50);

    int choice;  //variable to hold the selection

    showSelection();
    cin >> choice;

    while (choice != 9)
    {
        switch (choice)
        {
        case 1: 
            sellProduct(orange, counter);
            break;
        case 2: 
            sellProduct(apple, counter);
            break;
        case 3: 
            sellProduct(mango, counter);
            break;
        case 4: 
            sellProduct(strawberryBanana, counter);
            break;
		case 5:
			sellProduct(cranApple, counter);
			break;
		case 6:
			sellProduct(cranGrape, counter);
			break;
		case 7:
			sellProduct(crackers, counter);
			break;
		case 8:
			sellProduct(chips, counter);
			break;
        default: 
            cout << "Invalid selection." << endl;
        }//end switch

        showSelection();
        cin >> choice;
    }//end while
	
	dispenserType* arr[NUMBEROFDISPENSERS] = {&orange, &apple, &mango, &strawberryBanana, &cranApple, &cranGrape, &crackers, &chips}; // array of pointers to our dispensers
	fileReport(counter, arr);

	system("pause");
    return 0;
}//end main

void showSelection()
{
    cout << "*** Welcome to Shelly's Juice Shop ***" << endl;
    cout << "To select an item, enter " << endl;
    cout << "1 for orange" << endl;
    cout << "2 for mango" << endl; //Actually sells apple? I'd fix it, and the following errors, but that's not part of the requirements
    cout << "3 for strawberry banana" << endl;//Actually sells mango
    cout << "4 for Cookies" << endl;//Actually sells StrawberryBanana
	cout << "5 for CranApple juice" << endl;
	cout << "6 for CranGrape juice" << endl;
	cout << "7 for Crackers" << endl;
	cout << "8 for Chips" << endl;
    cout << "9 to exit" << endl;
}//end showSelection

void sellProduct(dispenserType& product, 
                 cashRegister& pCounter)
{
	int input;  //variable to temporarily the user input
    int amount = 0;  //variable to hold the amount entered

    if (product.getNoOfItems() > 0) //if the dispenser is not 
                                    //empty
    {
		bool needMoney = 1;
		while (needMoney == 1) //Needs money, or a cancel 
		{
			cout << "Please deposit a total of " << product.getCost()
				<< " cents, or hit [0] to cancel" << endl;
			cin >> input;
			bool valid = checkCurrency(input);

			if (valid == 0)
			{
				cout << "Please insert only standard US currency less than $5.";
			}
			else
			{
				amount = amount + input;
				if (input == 0)
				{
					cout << "Purchase Canceled.";
					needMoney = 0;
				}
				else if (amount < product.getCost())
				{
					cout << "Please deposit another "
						<< product.getCost() - amount
						<< " cents" << endl;
				}
				else if (amount >= product.getCost())
				{
					int temp = product.getCost(); //Holds the product's cost

					if ((amount - product.getCost()) > pCounter.getCurrentBalance()) //Checks if the change needed is greater than the current balance
					{
						cout << "Warning: Not enough cash on hand to dispense change." << endl <<
							"Please collect your item below and weep for your lost " << amount - temp << " cents change, then enjoy." << endl;
					}
					else
					{
						temp = amount - temp; // Changes temp into a counter for change needed
						pCounter = pCounter - temp; //.acceptAmount(amount); //Removes the change from the til
						cout << "Collect your item and " << temp << " cents change at the bottom, then enjoy." << endl;
					}

					pCounter = pCounter + amount; //.acceptAmount(amount); //Adds the input amount to the register
					product--; //product.makeSale(); //Decrements the number of products

					cout << "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
						<< endl << endl;

					needMoney = 0;
				}

			}
		}//End While
    }
    else
        cout << "Sorry, this item is sold out." << endl;
}//end sellProduct

bool checkCurrency(int amount) //Checks if an input is a standard unit of currency
{
	bool valid = false; //Assume false
	if (amount == 5 || amount == 10 || amount == 25 || amount == 50 || amount == 100 || amount == 500 || amount == 0) //Checks for standard currencies, nickels, dimes, quarters, half-dollars, dollars and five dollars. Also checks for the exit code of 0
	{
		valid = true;
		return valid;
	}
	return valid;


}//end checkCurrency

void fileReport(cashRegister& pCounter, dispenserType* array[]) //Creates a report that shows what was sold, the status of each dispenser, and how much currency the register contains
{
	cout << "REPORT:" << endl;
	cout << "CASH REGISTER CONTAINS: " << pCounter.getCurrentBalance() << " CENTS." << endl;
	for (int x = 0; x < NUMBEROFDISPENSERS; x++) //Operates on each dispenser in our array, prints its status
	{
		cout << "DISPENSER [" << x + 1 << "] STATUS:" << endl;
		cout << "ITEMS LEFT: " << array[x]->getNoOfItems() << endl;
		cout << "ITEMS SOLD: " << array[x]->getNoOfSoldItems() << endl;
	}


	string filename = "JM-Report.txt";

	ofstream outfile((filename.c_str())); //Creates a text file we're going to save everything to 

	if (outfile.is_open())
	{
		outfile << "CASH REGISTER CONTAINS: " << pCounter.getCurrentBalance() << " cents." << endl;

		for (int x = 0; x < NUMBEROFDISPENSERS; x++) //Operates on each dispenser in our array, prints its status
		{
			outfile << "DISPENSER [" << x + 1 << "] STATUS:" << endl;
			outfile << "ITEMS LEFT: " << array[x]->getNoOfItems() << endl;
			outfile << "ITEMS SOLD: " << array[x]->getNoOfSoldItems() << endl;
		}
		outfile << endl;
		outfile.close(); //Closes the finished file
		cout << "REPORT WRITTEN TO FILE \"JM-Report.txt\"" << endl;
	}
	else cout << "Unable to open file for writing" << endl;

}//end fileReport